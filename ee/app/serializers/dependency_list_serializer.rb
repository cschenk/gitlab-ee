# frozen_string_literal: true

class DependencyListSerializer < BaseSerializer
  entity DependencyListEntity
end
